VERSION = r8
BINNAME = kitten
EXTENSION = .z5

INFORM = inform
PERL = perl

DISTNAME = $(BINNAME)-$(VERSION)
DISTDIR = $(DISTNAME)

$(BINNAME): nki
	$(INFORM) $(BINNAME).inf

nki:
	$(PERL) nki2inf.pl *.nki > nki.inf

dist: distclean
	git archive --format=tgz --prefix $(DISTDIR)/ HEAD -o $(DISTDIR).tar.gz
	@echo
	@echo "$(DISTDIR).tar.gz created"
	@echo

clean:
	rm -f *core *sav *$(EXTENSION) nki.inf

distclean: clean
	rm -rf $(DISTDIR)
	rm -f $(DISTDIR).tar $(DISTDIR).tar.gz

